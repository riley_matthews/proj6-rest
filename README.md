# ACP Time Calculator with MongoDB

Used for calculating RUSA ACP times.

Each time a distance is filled in, the corresponding open and close times are displayed.

On selecting the "Submit" button, the calculated open and close times will be sent to the database. If previous entries existed in the
database, they will be deleted and replaced with the new entries. The page is then refreshed.

On selecting the "Display" button, a new tab will open displaying the database entries.

After ensuring that docker is installed, execute `docker-compose up --build` to build and run the containers.
Navigate to: `http://127.0.0.1:<PORTA>/` to see the calculator main page.

**Note:** <PORTA> value is set in the docker-compose.yml file. Initially set to 5001

## The Consumer Website

A separate service website functions as a way to expose brevet times that are stored in the database. The website calls API's to obtain the 
information requested from the user. The following types of requests can be made:

- List All open and close times 
- List only the open times
- List only the close times
- An optional parameter can be used to only display data for the first "n" control points. If left blank, all points will be displayed.

Navigate to: `http://127.0.0.1:<PORTB>/` to see the consumer website.

The API's used by the consumer website are implemented in the /brevets/flask_brevets.py file under the "API" section.
They are implemented using flask-restful methods.

To see the raw return data from the API's, navigate to the following:

- `http://127.0.0.1:<PORTA>/listAll`
- `http://127.0.0.1:<PORTA>/listOpenOnly`
- `http://127.0.0.1:<PORTA>/listCloseOnly`
- Add /json or /csv to the queries to specifiy which format. Ex: `http://127.0.0.1:<PORTA>/listAll/csv`
- json will return a raw list of JSON items. csv will return the values of the items requested separated by commas.
- Add a 'top' query parameter to return only the top n items. EX: `http://127.0.0.1:<PORTA>/listAll/csv?top=2`

**Note** <PORTB> value is set in the docker-compose.yml file. Initially set to 5000

## Basic Rules

Some of the basic guidelines for scheduling a brevet:

- The last control point must be at least the brevet distance.
- The last control point must be less than 20% longer than the brevet distance.
- The first control point after the start must be at least 60km from the start (explained in domain logic section).
- See Domain Logic section for calculation algorithm.

## Domain Logic

ACP time calculations are performed by the funtions located in the /brevets/acp_times.py file.

The algorithm in these functions was developed using the guidelines from the following pages:

- https://rusa.org/pages/acp-brevet-control-times-calculator
- https://rusa.org/pages/rulesForRiders
- https://rusa.org/pages/orgreg

Test cases for the functions are located in the /brevets/test_cases.py file.
Tests can be run by changing to the /brevets/ directory and typing "nosetests" in the command line.

**Note:** In the linked guidelines above, there is a special case for control points under 60KM. They say there is a special code to calculate relaxed control close times for the first 60KM. However this special code is not specified in their documentation. Because of this I have made a choice to not accept control point times of less than 60KM. Once the special code is identified it can be added. Thanks for understanding. Entering a control point of 45KM for example, will redirect the user to the invalid input screen (/brevets/templates/input_error.html).

## AJAX and Flask Implementation

The open and close times are displayed as soon as a control point distance is entered. This is done using
AJAX and Flask. 

The implementation of these can be found in the /brevets/flask_brevets.py and /brevets/templates/calc.html files.

Bad user input error checking is performed in the calc.html file.

## Database Implementation

The database is running in its own container, as specified in the docker-compose file. Our python application can import pymongo methods that
allow us to easily communicate between the containers as they are running.

See "routes for submit and display buttons" section of the flask_brevets.py file for the code to this process.

### Author(s):

Riley Matthews, rmatthe2@uoregon.edu or riley@cs.uoregon.edu

Credits to Michal Young and Ram Durairajan for the initial version of this code.