"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""

import arrow
from math import modf

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

min_speed = [15, 15, 15, 11.428, 13.333]
max_speed = [34, 32, 30, 28, 26]
closing_times = {200 : 13.5, 300 : 20, 400: 27, 600 : 40, 1000 : 75}


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    segments = [] # break the km into segments. e.g 550km -> [200, 200, 150]
    control_dist_km = round(control_dist_km)
    result = arrow.get(brevet_start_time)

    if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km

    x = control_dist_km
    while (x > 0):
      if x > 1000:
        segments.insert(0, (x - 1000))
        x = 1000

      elif x > 600:
        segments.insert(0, (x - 600))
        x = 600

      elif x > 400:
        segments.insert(0, (x - 400))
        x = 400

      elif x > 200:
        segments.insert(0, (x - 200))
        x = 200

      elif x > 0:
        segments.insert(0, x)
        x = 0

    # calculate total time by adding the segments together after applying
    # their respective rates.
    total_time = 0
    for i in range(len(segments)):
      total_time += (segments[i]/max_speed[i])

    total_time = modf(total_time)

    hrs = total_time[1]
    hrs = round(hrs)
    mins = total_time[0]
    mins *= 60
    mins = round(mins)

    return result.shift(hours=hrs, minutes=mins).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    segments = []
    control_dist_km = round(control_dist_km)
    result = arrow.get(brevet_start_time)

    if control_dist_km == 0:
      return result.shift(hours=1).isoformat()

    if control_dist_km > brevet_dist_km:
      # use standard closing time for that brevet distance
      return result.shift(hours=closing_times[brevet_dist_km]).isoformat()

    x = control_dist_km
    while (x > 0):
      if x > 1000:
        segments.insert(0, (x - 1000))
        x = 1000

      elif x > 600:
        segments.insert(0, (x - 600))
        x = 600

      elif x > 400:
        segments.insert(0, (x - 400))
        x = 400

      elif x > 200:
        segments.insert(0, (x - 200))
        x = 200

      elif x > 0:
        segments.insert(0, x)
        x = 0

    total_time = 0
    for i in range(len(segments)):
      total_time += (segments[i]/min_speed[i])

    total_time = modf(total_time)

    hrs = total_time[1]
    hrs = round(hrs)
    mins = total_time[0]
    mins *= 60
    mins = round(mins)

    return result.shift(hours=hrs, minutes=mins).isoformat()
