<html>
    <head>
        <title>CIS 322 REST-api proj6: display brevet times</title>
    </head>

    <body>
        <h1>Display Current Brevet Times</h1>
        <form action="display.php" method="post">
            <label for="type">Choose which times to display:</label>
            <select id="type" name="type">
                <option selected value="listAll">List all times</option>
                <option value="listOpenOnly">List open times only</option>
                <option value="listCloseOnly">List close times only</option>
            </select><br><br>
            <label for="count">How many of the control points would you like to display?</label><br>
            <input type="text" id="count" name="count"><br><br>
            <input type="submit" value="Submit">
        </form>
    </body>
</html>
